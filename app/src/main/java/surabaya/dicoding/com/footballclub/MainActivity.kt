package surabaya.dicoding.com.footballclub

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import surabaya.dicoding.com.footballclub.adapter.FootballAdapter
import surabaya.dicoding.com.footballclub.model.Football

/**
 * Created by handy on 03/09/18.
 * akang.handy95@gmail.com
 */
class MainActivity : AppCompatActivity() {

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val name = resources.getStringArray(R.array.club_name)
        val image = resources.obtainTypedArray(R.array.club_image)
        recyclerView.layoutManager = LinearLayoutManager(this)

        val list: ArrayList<Football> = arrayListOf()
        list.add(Football(name[0],image.getResourceId(0,0)))
        list.add(Football(name[1],image.getResourceId(1,0)))
        list.add(Football(name[2],image.getResourceId(2,0)))
        list.add(Football(name[3],image.getResourceId(3,0)))
        list.add(Football(name[4],image.getResourceId(4,0)))
        list.add(Football(name[5],image.getResourceId(5,0)))
        list.add(Football(name[6],image.getResourceId(6,0)))
        list.add(Football(name[7],image.getResourceId(7,0)))

        recyclerView.adapter = FootballAdapter(list) {
            startActivity<DescriptionActivity>("title" to it.title)
        }
    }
}