package surabaya.dicoding.com.footballclub

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import org.jetbrains.anko.setContentView
import surabaya.dicoding.com.footballclub.ui.DescriptionUI

/**
 * Created by handy on 13/09/18.
 * akang.handy95@gmail.com
 */

class DescriptionActivity : AppCompatActivity(){

    lateinit var descriptionUI: DescriptionUI
    lateinit var title : String

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        descriptionUI = DescriptionUI()
        descriptionUI.setContentView(this)
        val intent = intent
        title = intent.getStringExtra("title")

        if (title.equals("Barcelona FC")) {
            descriptionUI.teamName.text = title
            descriptionUI.teamDescription.text = "Barcelona mendengarkan Tentang suara ini, dikenal hanya sebagai Barcelona dan sehari-hari sebagai Barca ([ˈBaɾsə] ), adalah klub sepakbola profesional yangberbasis di Barcelona , Catalonia , Spanyol."
            Glide.with(this).load(R.drawable.img_barca).into(descriptionUI.teamBadge)
        } else if (title.equals("Real Madrid FC")){
            descriptionUI.teamName.text = title
            descriptionUI.teamDescription.text = "Manchester City Football Club is a football club in Manchester, England. Founded in 1880 as St. Mark's (West Gorton), it became Ardwick Association Football Club in 1887 and Manchester City in 1894. The club's home ground is the City of Manchester Stadium in east Manchester, to which it moved in 2003, having played at Maine Road since 1923."
            Glide.with(this).load(R.drawable.img_madrid).into(descriptionUI.teamBadge)
        } else if (title.equals("Bayern Munchen FC")) {
            descriptionUI.teamName.text = title
            descriptionUI.teamDescription.text = "Pertandingan pertama FC Bayern Munich melawan 1. FC Nürnberg pada tahun 1901 FC Bayern Munich didirikan oleh anggota klub senam Munich (MTV 1879). Ketika jemaat anggota MTV 1879 memutuskan pada 27 Februari 1900 bahwa para pemain klub tidak akan diizinkan untuk bergabung dengan Asosiasi Sepak Bola Jerman (DFB), 11 anggota divisi sepakbola meninggalkan sidang dan pada malam yang sama mendirikan Fußball- Klub Bayern München. Dalam beberapa bulan, Bayern mencapai kemenangan skor tinggi melawan semua rival lokal, termasuk kemenangan 15-0 melawan FC Nordstern"
            Glide.with(this).load(R.drawable.img_bayern).into(descriptionUI.teamBadge)
        } else if (title.equals("Manchester City FC")) {
            descriptionUI.teamName.text = title
            descriptionUI.teamDescription.text = "Manchester City Football Club is a football club in Manchester, England. Founded in 1880 as St. Mark's (West Gorton), it became Ardwick Association Football Club in 1887 and Manchester City in 1894. The club's home ground is the City of Manchester Stadium in east Manchester, to which it moved in 2003, having played at Maine Road since 1923."
            Glide.with(this).load(R.drawable.img_city).into(descriptionUI.teamBadge)
        } else if (title.equals("Manchester United FC")) {
            descriptionUI.teamName.text = title
            descriptionUI.teamDescription.text = "Manchester United Football Club is an English professional football club, based in Old Trafford, Greater Manchester that plays in the Premier League. Founded as Newton Heath LYR Football Club in 1878, the club changed its name to Manchester United in 1902 and moved to Old Trafford in 1910. They are regarded as one of the most successful clubs in English football.\n" +
                    "            Manchester United have won the most League titles (20) of any English club, a joint record 11 FA Cups, four League Cups, and a record 20 FA Community Shields. The club has also won three European Cups, one UEFA Cup Winners' Cup, one UEFA Super Cup, one Intercontinental Cup, and one FIFA Club World Cup. In 1998–99, the club won a continental treble of the Premier League, the FA Cup and the UEFA Champions League.\n" +
                    "            The 1958 Munich air disaster claimed the lives of eight players. In 1968, under the management of Matt Busby, Manchester United was the first English football club to win the European Cup. Alex Ferguson won 28 major honours, and 38 in total, from November 1986 to May 2013, when he announced his retirement after 26 years at the club. On 19 May 2014, Louis van Gaal was appointed as the club's new manager after Ferguson's successor David Moyes was sacked after only 10 months in charge, with the club's record appearance-maker, Ryan Giggs, appointed as his assistant after a brief period as caretaker manager.\n" +
                    "            Manchester United is the third-richest football club in the world for 2011–12 in terms of revenue, with an annual revenue of €395.9 million, and the world's second most valuable sports team in 2013, valued at \$3.165 billion. It is one of the most widely supported football teams in the world. After being floated on the London Stock Exchange in 1991, the club was purchased by Malcolm Glazer in May 2005 in a deal valuing the club at almost £800 million, after which the company was taken private again. In August 2012, Manchester United made an initial public offering on the New York Stock Exchange."
            Glide.with(this).load(R.drawable.img_mu).into(descriptionUI.teamBadge)
        } else if (title.equals("Chelsea FC")) {
            descriptionUI.teamName.text = title
            descriptionUI.teamDescription.text = "Chelsea Football Club /ˈtʃɛlsiː/ are a professional football club based in Fulham, London, who play in the Premier League, the highest level of English football. Founded in 1905, the club have spent most of their history in the top tier of English football. The club's home ground is the 41,837-seat Stamford Bridge stadium, where they have played since their establishment."
            Glide.with(this).load(R.drawable.img_chelsea).into(descriptionUI.teamBadge)
        } else if (title.equals("AC Milan FC")) {
            descriptionUI.teamName.text = title
            descriptionUI.teamDescription.text = "AC MIlan FC deskripsi"
            Glide.with(this).load(R.drawable.img_acm).into(descriptionUI.teamBadge)
        } else if (title.equals("Arsenal FC")) {
            descriptionUI.teamName.text = title
            descriptionUI.teamDescription.text = "Arsenal Football Club is a professional football club based in Holloway, London which currently plays in the Premier League, the highest level of English football. One of the most successful clubs in English football, they have won 13 First Division and Premier League titles and a joint record 11 FA Cups."
            Glide.with(this).load(R.drawable.img_arsenal).into(descriptionUI.teamBadge)
        }
    }
}