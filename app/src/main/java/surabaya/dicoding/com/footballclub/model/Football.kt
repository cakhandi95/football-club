package surabaya.dicoding.com.footballclub.model

/**
 * Created by handy on 03/09/18.
 * akang.handy95@gmail.com
 */

data class Football (var title: String, var imageView: Int)