package surabaya.dicoding.com.footballclub.ui

import android.view.Gravity
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import org.jetbrains.anko.*
import surabaya.dicoding.com.footballclub.R

/**
 * Created by handy on 11/09/18.
 * akang.handy95@gmail.com
 */
class ItemFootballUI : AnkoComponent<ViewGroup> {

    companion object {
        val ivImage = 1
        val tvTitleId = 2
    }

    override fun createView(ui: AnkoContext<ViewGroup>)= with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            padding = dip(16)
            orientation = LinearLayout.HORIZONTAL

            imageView {
                id = ivImage
            }.lparams{
                height = dip(50)
                width = dip(50)
            }

            textView {
                id = tvTitleId
                textSize = 16f
            }.lparams{
                margin = dip(15)
            }

        }
    }
}