package surabaya.dicoding.com.footballclub.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import org.jetbrains.anko.AnkoContext
import surabaya.dicoding.com.footballclub.model.Football
import surabaya.dicoding.com.footballclub.ui.ItemFootballUI

/**
 * Created by handy on 03/09/18.
 * akang.handy95@gmail.com
 */

class FootballAdapter (var list: ArrayList<Football> = arrayListOf(),var listener : (Football) -> Unit) : RecyclerView.Adapter<FootballAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FootballAdapter.ViewHolder {
        return ViewHolder(ItemFootballUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun onBindViewHolder(holder: FootballAdapter.ViewHolder, position: Int) {
        holder.bind(list[position],listener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private var tvtitle: TextView = itemView.findViewById(ItemFootballUI.tvTitleId) as TextView
        private var imgImage : ImageView = itemView.findViewById(ItemFootballUI.ivImage) as ImageView

        fun bind(football: Football,listener: (Football) -> Unit) {
            Glide.with(itemView.context)
                    .load(football.imageView)
                    .into(imgImage)

            tvtitle.text = football.title
            itemView.setOnClickListener { listener(football) }
        }
    }
}
