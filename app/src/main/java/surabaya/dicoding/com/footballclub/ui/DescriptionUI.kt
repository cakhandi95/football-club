package surabaya.dicoding.com.footballclub.ui

import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.swipeRefreshLayout
import surabaya.dicoding.com.footballclub.DescriptionActivity
import surabaya.dicoding.com.footballclub.R.color.*

/**
 * Created by handy on 13/09/18.
 * akang.handy95@gmail.com
 */

class DescriptionUI : AnkoComponent<DescriptionActivity> {

    lateinit var teamBadge : ImageView
    lateinit var teamName : TextView
    lateinit var teamDescription : TextView

    override fun createView(ui: AnkoContext<DescriptionActivity>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            orientation = LinearLayout.VERTICAL
            backgroundColor = Color.WHITE

                scrollView {
                    isVerticalScrollBarEnabled = false
                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent)

                        linearLayout{
                            lparams(width = matchParent, height = wrapContent)
                            padding = dip(10)
                            orientation = LinearLayout.VERTICAL
                            gravity = Gravity.CENTER_HORIZONTAL

                            teamBadge =  imageView {}.lparams(height = dip(75))
                            teamName = textView{
                                this.gravity = Gravity.CENTER
                                textSize = 15f
                                textColor = ContextCompat.getColor(context, colorBlank)
                            }.lparams{
                                topMargin = dip(5)
                            }
                            teamDescription = textView().lparams{
                                topMargin = dip(20)
                            }
                        }
                    }
                }
        }
    }
}